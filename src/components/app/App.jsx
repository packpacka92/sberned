import React, { Component } from 'react';
import style from './app.less';
import { Dialog } from '../../../lib';
import PaymentDialog from '../../containers/PaymentDialog.jsx';
import PaymentForm from '../../containers/PaymentForm.jsx';

class AppComponent extends Component {
  render() {
    return (
      <div>
        <PaymentDialog>
          <PaymentForm />
        </PaymentDialog>
        <div className={style.index}>
          <h3>
            {this.props.welcome}
          </h3>
          <button className={style.openDlgBtn} onClick={this.props.openDialog}>Подтвердить личность</button>
        </div>
        <div id="dialog_anchor"></div>
      </div>
    );
  }
}


export default AppComponent;
