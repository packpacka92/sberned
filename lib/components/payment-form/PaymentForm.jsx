import React, {Component, PropTypes} from 'react';
import styles from './payment-form.less';
import visaIcon from '../images/visa.png';
import okIcon from '../images/ok.svg';

/**
 * @class Класс компонента формы проверки данных карты
 */
class PaymentForm extends Component {
    renderAction() {
        let verified = this.props.verified;
        if(verified) {
            return  <span className={styles.verifiedBtn}><img className={styles.verifiedIcon} src={okIcon}/>Подверждено</span>;
        }
        return  <button type="submit" className={styles.verifyBtn}>Подтведить</button>;
    }
    runVerify(e) {
        e.preventDefault();
        if(!this.refs.form.checkValidity()) {
            return false;                 
        }
        if(this.props.verifyPayment) {
            var data = {
                cardNumber: this.refs.form['card_number'].value,
                month: this.refs.form['card_month'].value,
                year: this.refs.form['card_year'].value,
                ownerName: this.refs.form['card_owner'].value,
                cvv: this.refs.form['card_cvv'].value
            };
            this.props.verifyPayment(data);
        }
        return true;
    }
    componentDidMount() {
        this.refs.form.reset();
    }
    render() {
        return (
            <form className="payment-form" ref="form" onSubmit={(e) => this.runVerify(e)}>
                <div className={styles.welcomeMessage}>
                    Пожалуйста, введите данные Вашей банковской карты
                </div>
                <div className={styles.validationMessage} style={{display: this.props.invalid? '': 'none' }}>
                    Ошибка! Возможно:<br/>
                    &mdash;&nbsp;Вы ошиблись в реквизитах карты<br/>
                    &mdash;&nbsp;На карте нулевой баланс или она заблокирована.<br/>
                    Проверьте реквизиты или укажите данные другой карты.
                </div>
                <div className={styles.field}>
                    <div className={styles.fieldName}>Номер карты</div>
                    <div className={styles.fieldInput}>
                        <div className={styles.cardNumber}>
                            <input type="text" name="card_number" placeholder="1111 2222 3333 4444" required pattern="([0-9]{4}\s){3}[0-9]{4}" className={styles.input} />
                            <img src={visaIcon} className={styles.visaIcon}></img>
                        </div>
                    </div>
                </div>  
                <div className={styles.field}>
                    <div className={styles.fieldName}>Действует до</div>
                    <div className={styles.fieldInput}>
                        <input type="text" name="card_month" pattern="[0-9][0-2]" required placeholder="MM" className={styles.input + ' ' + styles.shortInput}/>
                        <span className={styles.slashDivider}>/</span>
                        <input type="text" name="card_year" placeholder="YY" pattern="[0-9][0-9]" required className={styles.input  + ' ' + styles.shortInput}/>
                    </div>
                </div>
                <div className={styles.field}>
                    <div className={styles.fieldName}>Имя и фамилия</div>
                    <div className={styles.fieldInput}>
                        <input type="" name="card_owner" placeholder="IVAN IVANOV" required pattern="([a-zA-Z]+)\s?([a-zA-Z]+)?" className={styles.input}/>
                        <div className={styles.fieldComment}>Латинские буквы, как на карте</div>
                    </div>
                </div>
                <div className={styles.field}>
                    <div className={styles.fieldName}>Код CVV</div>
                    <div className={styles.fieldInput}>
                        <input type="password" name="card_cvv" pattern="[0-9]{3}" required className={styles.input + ' ' + styles.shortInput}/>
                        <div className={styles.fieldComment}>Три цифры с обратной стороны карты</div>
                    </div>
                </div>
                <div className={styles.buttonsBlock}>
                    {this.renderAction()}
                </div>
            </form>
        );
    }
}
PaymentForm.propTypes = {
    verifyPayment: PropTypes.func.isRequired,
    invalid: PropTypes.bool,
    verified: PropTypes.bool
};
export default PaymentForm;