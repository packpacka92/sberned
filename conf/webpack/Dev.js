'use strict';

/**
 * Development configuration.
 */
const WebpackBaseConfig = require('./Base');

class WebpackDevConfig extends WebpackBaseConfig {

    constructor() {
        super();
        this.config = {
            devtool: 'cheap-module-source-map',
            watch: true
        };
    }
}

module.exports = WebpackDevConfig;
