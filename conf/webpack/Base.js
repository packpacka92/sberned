'use strict';

/**
 * Webpack configuration base class
 */
const path = require('path');
const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const npmBase = path.join(__dirname, '../../node_modules');

class WebpackBaseConfig {

  constructor() {
    this._config = {};
  }

  /**
   * Get the list of included packages
   * @return {Array} List of included packages
   */
  get includedPackages() {
    return [].map((pkg) => path.join(npmBase, pkg));
  }

  /**
   * Set the config data.
   * This will always return a new config
   * @param {Object} data Keys to assign
   * @return {Object}
   */
  set config(data) {
    this._config = Object.assign({}, this.defaultSettings, data);
    return this._config;
  }

  /**
   * Get the global config
   * @param {Object} config Final webpack config
   */
  get config() {
    return this._config;
  }

  /**
   * Get the environment name
   * @return {String} The current environment
   */
  get env() {
    return 'dev';
  }

  /**
   * Get the absolute path to src directory
   * @return {String}
   */
  get srcPathAbsolute() {
    return path.resolve('./src');
  }
  
  
  /**
   * Get the absolute path to components directory
   * @return {String}
   */
  get libPathAbsolute() {
    return path.resolve('./lib');
  }
  
  
  
  /**
   * Get the absolute path to tests directory
   * @return {String}
   */
  get testPathAbsolute() {
    return path.resolve('./test');
  }
  
  /**
   * Get the absolute path to build directory
   * @return {String}
   */
  getBuildPathAbsolute() {
    return path.join(path.resolve('.'), 'build');
  }
  
  getPlugins() {
      return [
           new webpack.NoErrorsPlugin(),
           new HtmlWebpackPlugin({
                    template: path.join(this.srcPathAbsolute, 'index.html'),
                    filename: path.join(this.getBuildPathAbsolute(), 'index.html')
                })
      ];
     
  }
  /**
   * Get the default settings
   * @return {Object}
   */
  get defaultSettings() {
    return {
      // context: this.srcPathAbsolute,
      devtool: 'eval',
      entry: this.srcPathAbsolute +  '/index.js',
      module: {
        loaders: [
          {
            test: /\.(less|css)$/i,
            loaders: [
              'style',
              'css?modules&importLoaders=1&localIdentName=[name]_[local]__[hash:base64:5]',
              {
                loader: 'postcss',
                options: {
                  plugins: function () {
                    return [
                      require('autoprefixer')
                    ];
                  }
                }
              },
              'less'
            ]
          },
          {
            test: /\.(png|jpg|gif|mp4|ogg|svg|woff|woff2)$/,
            loaders: ['file']
          },
          {
            test: /\.json$/,
            loaders: 'json'
          },
          {
            test: /\.(js|jsx)$/,
            include: [].concat(
              this.includedPackages,
              [this.srcPathAbsolute, this.libPathAbsolute]
            ),
            loader: 'babel-loader',
            query: {
              presets: ['react', 'es2015']
            }
          }
        ]
      },
      output: {
        path: path.join(this.getBuildPathAbsolute(), 'assets'),
        filename: 'app.js',
        publicPath: './assets/'
      },
      plugins: this.getPlugins()
    };
  }
}


module.exports = WebpackBaseConfig;
