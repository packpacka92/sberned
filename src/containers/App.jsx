import React, { Component, PropTypes } from 'react';
import * as appActions from '../redux/app/app.actions.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AppComponent from '../components/app/App.jsx';


function mapStateToProps(state) {
  let props = {
    welcome: state.welcome
  };

  return props;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(appActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
