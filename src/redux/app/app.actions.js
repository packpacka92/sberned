import {createAction} from 'redux-actions';
import * as actionTypes from './app.action-types';

export const openDialog = createAction(actionTypes.OPEN_DIALOG);
export const closeDialog = createAction(actionTypes.CLOSE_DIALOG);