'use strict';

/**
 * Production configuration.
 */

const webpack = require('webpack');
const path = require('path');
const WebpackBaseConfig = require('./Base');

class WebpackDistConfig extends WebpackBaseConfig {

    constructor() {
        super();
        this.config = {
            cache: false,
            devtool: false,
            plugins: [
                ...this.getPlugins(),
                new webpack.DefinePlugin({
                    'process.env.NODE_ENV': '"production"'
                })
            ]
        };
    }
    getBuildPathAbsolute() {
        return path.join(path.resolve('.'), 'dist');
    }
    get env() {
        return 'dist';
    }
}

module.exports = WebpackDistConfig;
