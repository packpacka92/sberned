import Dialog from './components/dialog/Dialog.jsx'; 
import PaymentForm from './components/payment-form/PaymentForm.jsx';

export {
    Dialog,
    PaymentForm
};


