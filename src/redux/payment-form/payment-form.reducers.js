import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import * as actions from './payment-form.actions';

const validData = {
    year: '12',
    month: '12',
    ownerName: 'IVAN IVANOV'
};

function validateData(data) {
    console.log(data);
    if (!data) {
        return false;
    }
    let valid = true; 
    console.log( Object.keys(validData));
    Object.keys(validData).forEach(function(key) {
        console.log(data[key]);
        console.log(validData[key]);
        console.log('------------');
        if (data[key] != validData[key]) {
            valid = false;
        }
    });
    return valid;
}

export default handleActions({
    [actions.verifyPayment]: function(state, action) {
        var valid = validateData(action.payload);
        return Object.assign({}, state, {
            invalid: !valid,
            verified: valid
        });
    }
}, {
    invalid: false,
    verified: false
});
