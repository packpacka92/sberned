import { createStore } from 'redux';
import createLogger from 'redux-logger';
import { applyMiddleware } from 'redux';

function middlewares() {
    var logger = createLogger();
    return applyMiddleware(logger);
}

function reduxStore(reducers, initialState) {
    const store = createStore(reducers, middlewares());
    return store;
}

export default reduxStore;
