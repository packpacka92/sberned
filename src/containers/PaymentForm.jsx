import React, { Component, PropTypes } from 'react';
import * as actions from '../redux/payment-form/payment-form.actions.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PaymentForm } from '../../lib';


function mapStateToProps(state) {
  return state.paymentForm;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentForm);
