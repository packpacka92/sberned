import React, { Component, PropTypes } from 'react';
import ReactDom from 'react-dom';
import styles from './dialog-portal.less';

/**
 * @class Класс портала для вывода диалогового окна
 * По задумке должен быть нормальным порталом, который аппендит диалог куда-то ближе к body
 * Но пока так. Для демо хватит.
 */
class DialogPortal extends Component {
    render() {
        return (
            <div style={this.props.style} className="dialog-portal">
                <div className={styles.overlay}></div>
                {this.props.children}
            </div>
        );
    }
}

DialogPortal.propTypes = {
    portalId: PropTypes.string.isRequired
};

export default DialogPortal;
