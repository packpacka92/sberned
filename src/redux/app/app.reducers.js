import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import * as actions from './app.actions';
import paymentForm from '../payment-form/payment-form.reducers';


function welcome(action) {
    return 'Привет, %username%!';
}

const dialog = handleActions({
    [actions.openDialog]: (state) => Object.assign({}, state, {isOpen: true}),
    [actions.closeDialog]: (state) => Object.assign({}, state, {isOpen: false})
}, {
    isOpen: false
});

export default combineReducers({
    welcome,
    dialog,
    paymentForm
});
