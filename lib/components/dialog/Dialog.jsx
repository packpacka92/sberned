import React, {Component, PropTypes} from 'react';
import styles from './dialog.less';
import DialogPortal from './DialogPortal.jsx';
import closeIcon from '../images/close.png';

var portalsCount = 0;
function nextPortalId() {
    return 'dialog_portal_' + (portalsCount++);
}

/**
 * @class Класс компонента, выводящего диалог
 */
class Dialog extends Component {
    constructor(props, ...other) {
        super(props, ...other);
        this.state = this.getNewState(props);
        this.portalId = nextPortalId();
    }
    getNewState(props) {
        return {
            showDialog: props.isOpen
        };
    }
    createHeader() {
        return (
            <div className={styles.header}>
                <div className={styles.headerTitle}>{this.props.header}</div>
                <img src={closeIcon} className={styles.headerClose} onClick={() => this.close()} />
            </div>
        );
    }
    close() {
        if(this.props.closeHandler){
            this.props.closeHandler(this);
        } else {
            this.setState({showDialog: false});
        }
    }
    createBody() {
        return (
            <div className={styles.body}>
                {this.props.children}
            </div>
        );
    }
    componentWillReceiveProps(newProps) {
        this.setState(this.getNewState(newProps));
    }
    render() {
        var styles = {};
        if(!this.state.showDialog) {
            styles['display'] = 'none';
        }
        return (
            <DialogPortal style={styles} portalId={this.portalId}>
                <div className="dialog">
                    {this.createHeader()}
                    {this.createBody()}
                </div>
            </DialogPortal>
        );
    }
}
Dialog.propTypes = {
    closeHandler: PropTypes.func,
    isOpen: PropTypes.bool.isRequired

};
export default Dialog;