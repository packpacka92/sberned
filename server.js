var express = require('express'),
	app = express(),
	argv = require('yargs').argv,
	path = require('path'),
	port = argv.port || 5000,
    directory = argv.root || 'build';

app.listen(port, function() {
	console.log(`Express server listening on port ${port}. Root is /${directory}`);
});

app.use(express.static(path.join(__dirname,`/${directory}`)));