'use strict';

import 'babel-polyfill';

const testsContext = require.context('.', true, /(Test\.(js|jsx)$)/);
testsContext.keys().forEach(testsContext);
