import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Dialog } from '../../lib';
import PaymentForm from './PaymentForm.jsx';
import * as appActions from '../redux/app/app.actions.js';


class PaymentDialog extends Dialog {
  render() {
    return (
      <Dialog
        isOpen={this.props.isOpen}
        closeHandler={this.props.closeDialog}
        header="Подтверждение личности">
          {this.props.children}
      </Dialog>
    );
  }
}

function mapStateToProps(state) {
  return state.dialog;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      closeDialog: appActions.closeDialog 
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentDialog);
