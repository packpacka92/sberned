import React from 'react';
import { shallow } from 'enzyme';
import { Dialog } from '../../../lib';

//todo: Написать тесты

describe('<Dialog />', () => {

  let component;
  beforeEach(function() {
    component = shallow(<Dialog />);
  });

  describe('when rendering the component', function() {

    it('should have a className of "dialog-portal"', function() {
        expect(component.hasClass('dialog-portal')).to.equal(true);
    });
  });
});
