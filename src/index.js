import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './stores';
import appReducer from './redux/app/app.reducers.js';
import App from './containers/App.jsx';

const store = configureStore(appReducer);

render(
  <Provider store={store}>
    <App />
  </Provider>,

  document.getElementById('app')
);
