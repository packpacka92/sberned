import {createAction} from 'redux-actions';
import * as actionTypes from './payment-form.action-types';

export const verifyPayment = createAction(actionTypes.VERIFY_PAYMENT_DATA);